﻿/* COPYRIGHT REMARK
 * 
 * @file			PlayerController.cs
 * @summary			Controls the player
 * @author			Malte "TheGejr" Gejr Korup
 * @contact			www.mkkvk.dk/kontakt
 *  
 * @copyright		Copyright (C) 2016 - 2017  -- Malte "TheGejr" Gejr Korup
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using LudumDare;

namespace LudumDare
{
    public class PlayerController : MonoBehaviour
    {

        public float mouseSensitivityX = 1;
        public float mouseSensitivityY = 1;
        public float walkSpeed = 6;
        public float jumpVelocity = 200;
		public float jumpSpeed = 6;
        public LayerMask groundedMask;
		public float _timeHeld = 0.0f;

		public float distToTargetedPlanet;

		public GameObject currentGround;
		private GameObject temp;

        //System variables
        public bool grounded;
        Vector3 moveAmount;
        Vector3 smoothMoveVelocity;
        float verticalLookRotation;
        Transform camaraTransform;
        Rigidbody rb;

        void Awake()
        {
            //Cursor.lockState = CursorLockMode.Locked;
            //Cursor.visible = false;
			rb = GetComponent<Rigidbody>();
        }

        void Update()
        {
            //Movement
            float inputX = Input.GetAxisRaw("Horizontal");
            float inputY = Input.GetAxisRaw("Vertical");

            Vector3 moveDir = new Vector3(inputX, 0, inputY).normalized;
            Vector3 targetMoveAmount = moveDir * walkSpeed;
            moveAmount = Vector3.SmoothDamp(moveAmount, targetMoveAmount, ref smoothMoveVelocity, 0.15f);

            //Jump
			if (Input.GetKeyDown (KeyCode.Space))
			{
				_timeHeld = 0f;
			}
			if (Input.GetKey(KeyCode.Space))
			{
				_timeHeld += Time.deltaTime * jumpSpeed;
			}
			if(Input.GetKeyUp(KeyCode.Space))
			{
				Jump ();
			}

            //Grounded
            Ray ray = new Ray(transform.position, -transform.up);
			Ray ray2 = new Ray (transform.position, transform.up);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 1 + 0.1f, groundedMask))
            {
                grounded = true;
				temp = hit.transform.gameObject;
            }
			else if(Physics.Raycast(ray2, out hit, 1 + 0.1f, groundedMask))
			{
				grounded = true;
				temp = hit.transform.gameObject;
			}
            else
            {
                grounded = false;
            }
        }

		public void Jump()
		{
			if (grounded)
			{
				rb.AddForce(transform.up * jumpVelocity * _timeHeld);
			}
		}

        private void FixedUpdate()
        {
			currentGround = temp;
            //Anvender bevægelserne til rigidbody
			rb.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionZ;

            Vector3 localMove = transform.TransformDirection(moveAmount) * Time.fixedDeltaTime;
			rb.MovePosition(rb.position + localMove);
        }
    }
}