﻿/* COPYRIGHT REMARK
 * 
 * @file			Thanks.cs
 * @summary			Thansk message, and a way back to `LevelSelect.cs`
 * @author			Malte "TheGejr" Gejr Korup 
 * @contact			www.mkkvk.dk/kontakt
 *  
 * @copyright		Copyright (C) 2016 - 2017  -- Malte "TheGejr" Gejr Korup
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Thanks : MonoBehaviour
{

    public Button againButton;

    void OnEnable()
    {
        againButton.onClick.AddListener(delegate
        {
            OnAgainButtonPress();
        });
    }

    void OnAgainButtonPress()
    {
        SceneManager.LoadScene("ChooseLevel");
    }
}
