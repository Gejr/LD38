﻿/* COPYRIGHT REMARK
 * 
 * @file			LevelSelect.cs
 * @summary			You can choose which difficulty you want to play including a tutorial
 * @author			Malte "TheGejr" Gejr Korup 
 * @contact			www.mkkvk.dk/kontakt
 *  
 * @copyright		Copyright (C) 2016 - 2017  -- Malte "TheGejr" Gejr Korup
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelSelect : MonoBehaviour {

    public Button tutorialButton;
    public Button easyButton;
    public Button mediumButton;
    public Button hardButton;

    void OnEnable()
    {
        tutorialButton.onClick.AddListener(delegate
        {
            OnTutorialButtonPress();
        });

        easyButton.onClick.AddListener(delegate
        {
            OnEasyButtonPress();
        });

        mediumButton.onClick.AddListener(delegate
        {
            OnMediumButtonPress();
        });

        hardButton.onClick.AddListener(delegate
        {
            OnHardButtonPress();
        });
    }

    void OnTutorialButtonPress()
    {
        SceneManager.LoadScene("t_lvl01");
    }

    void OnEasyButtonPress()
    {
        SceneManager.LoadScene("e_lvl01");
    }

    void OnMediumButtonPress()
    {
        SceneManager.LoadScene("m_lvl01");
    }

    void OnHardButtonPress()
    {
        SceneManager.LoadScene("h_lvl01");
    }
}
