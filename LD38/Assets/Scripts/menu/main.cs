﻿/* COPYRIGHT REMARK
 * 
 * @file			main.cs
 * @summary			Acts as a camera mover and press any key to contiue start screen
 * @author			Malte "TheGejr" Gejr Korup
 * @contact			www.mkkvk.dk/kontakt
 *  
 * @copyright		Copyright (C) 2016 - 2017  -- Malte "TheGejr" Gejr Korup
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class main : MonoBehaviour {

    Canvas canvas;
    public Text temp;
    Camera mainCamera;

    private float speed = .125f;
    private float startTime;
    private float journeyLength;

    private Vector3 behindMark = new Vector3(0, 0, -0.3f);

    void Start()
    {
        canvas = GameObject.FindGameObjectWithTag("Canvas").GetComponent<Canvas>();
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();

        startTime = Time.time;
        journeyLength = Vector3.Distance(mainCamera.transform.position, behindMark);
    }

	void Update ()
    {
        if (Input.anyKeyDown)
        {
            canvas.enabled = false;
        }

        if (mainCamera.transform.position == behindMark)
        {
            SceneManager.LoadScene("ChooseLevel");
        }
	}

    void LateUpdate()
    {
        if(canvas.enabled == false)
        {
            Move();
        }
    }

    void Move()
    {
        float distCovered = (Time.time - startTime) * speed;
        float fracJourney = distCovered / journeyLength;
        mainCamera.transform.position = Vector3.Lerp(mainCamera.transform.position, behindMark, fracJourney);
    }
}
