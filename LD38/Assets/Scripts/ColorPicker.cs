﻿/* COPYRIGHT REMARK
 * 
 * @file			ColorPicker.cs
 * @summary			Gives the SpriteRenderer a `random`(pre-defined) color from an array 
 * @author			Malte "TheGejr" Gejr Korup
 * @contact			www.mkkvk.dk/kontakt
 *  
 * @copyright		Copyright (C) 2016 - 2017  -- Malte "TheGejr" Gejr Korup
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorPicker : MonoBehaviour {

	private SpriteRenderer spriteRenderer;
	public Color[] colors;

	void Start ()
	{
		spriteRenderer = GetComponent<SpriteRenderer> ();
		spriteRenderer.color = colors [Random.Range (0, colors.Length)];
	}
}
