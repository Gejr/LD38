﻿/* COPYRIGHT REMARK
 * 
 * @file			Portal.cs
 * @summary			Checks if the player enters a portal, and teleports the player to the next level
 * @author			Malte "TheGejr" Gejr Korup
 * @contact			www.mkkvk.dk/kontakt
 *  
 * @copyright		Copyright (C) 2016 - 2017  -- Malte "TheGejr" Gejr Korup
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Portal : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "PlayerCol")
        {
            if(SceneManager.GetActiveScene().name == "t_lvl01")
                SceneManager.LoadScene("ChooseLevel");

            if (SceneManager.GetActiveScene().name == "e_lvl01")
                SceneManager.LoadScene("e_lvl02");

            if (SceneManager.GetActiveScene().name == "m_lvl01")
                SceneManager.LoadScene("m_lvl02");

            if (SceneManager.GetActiveScene().name == "h_lvl01")
                SceneManager.LoadScene("h_lvl02");

            if (SceneManager.GetActiveScene().name == "e_lvl02")
                SceneManager.LoadScene("e_lvl03");

            if (SceneManager.GetActiveScene().name == "m_lvl02")
                SceneManager.LoadScene("m_lvl03");

            if (SceneManager.GetActiveScene().name == "h_lvl02")
                SceneManager.LoadScene("h_lvl03");

            if (SceneManager.GetActiveScene().name == "e_lvl03")
                SceneManager.LoadScene("Thanks");

            if (SceneManager.GetActiveScene().name == "m_lvl03")
                SceneManager.LoadScene("Thanks");

            if (SceneManager.GetActiveScene().name == "h_lvl03")
                SceneManager.LoadScene("Thanks");

        }
    }
}
