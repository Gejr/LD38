﻿/* COPYRIGHT REMARK
 * 
 * @file			GravityObject.cs
 * @summary			Adds the object(this is attached to) gravity thats defined in `GravityWell.cs`
 * @author			Malte "TheGejr" Gejr Korup
 * @contact			www.mkkvk.dk/kontakt
 *  
 * @copyright		Copyright (C) 2016 - 2017  -- Malte "TheGejr" Gejr Korup
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LudumDare;

namespace LudumDare
{
	[RequireComponent (typeof(Rigidbody))]
	public class GravityObject : MonoBehaviour
	{
		public float gravityModifier = 1.0f;
		Rigidbody rb;
		GravityWell gravityWell;

		void Awake()
		{
			gravityWell = GameObject.FindGameObjectWithTag ("GravityWell").GetComponent<GravityWell> ();
			rb = GetComponent<Rigidbody> ();
			rb.useGravity = false;
			rb.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezePositionZ;
		}

		void Start()
		{
			GetComponent<Rigidbody>().useGravity = false;
			GravityWell.AddObject (this);
		}

		void OnDestroy()
		{
			GravityWell.RemoveObject (this);
		}

		void FixedUpdate()
		{
			gravityWell.GravitationalPull (rb);
		}
	}
}