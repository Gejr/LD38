﻿/* COPYRIGHT REMARK
 * 
 * @file			LevelEnder_e.cs
 * @summary			Ends the easy levels after a defined ammount of time
 * @author			Malte "TheGejr" Gejr Korup 
 * @contact			www.mkkvk.dk/kontakt
 *  
 * @copyright		Copyright (C) 2016 - 2017  -- Malte "TheGejr" Gejr Korup
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelEnder_e : MonoBehaviour
{
    Text timeText;
    private int timeLeft = 120;

	void Start () {
        timeText = GameObject.FindGameObjectWithTag("timeText").GetComponent<Text>();
        StartCoroutine(end());
        StartCoroutine(timeDecrease());
        timeText.text = "<b>Time Left: " + timeLeft + "</b>";
	}

    IEnumerator timeDecrease()
    {
        timeLeft -= 1;
        timeText.text = "<b>Time Left: " + timeLeft + "</b>";
        yield return new WaitForSeconds(1);
        StartCoroutine(timeDecrease());
    }
	
    IEnumerator end()
    {
        yield return new WaitForSeconds(120);
        Time.timeScale = 0;
    }
}
