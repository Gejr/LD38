﻿/* COPYRIGHT REMARK
 * 
 * @file			GravityWell.cs
 * @summary			Creates a gravityfield, it has a `GravitationalPull` which does so the player's feet always point to the center of the planet
 * @author			Malte "TheGejr" Gejr Korup
 * @contact			www.mkkvk.dk/kontakt
 *  
 * @copyright		Copyright (C) 2016 - 2017  -- Malte "TheGejr" Gejr Korup
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LudumDare;

namespace LudumDare
{
	public class GravityWell : MonoBehaviour
	{
		PlayerController player;
		public float gravity = 9.8f;
		public float radius = 5.0f;

		private static List<GravityObject> objects = new List<GravityObject>();

		void Awake()
		{
			player = GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerController> ();
		}

		public static void AddObject (GravityObject newObject)
		{
			objects.Add (newObject);
		}

		public static void RemoveObject (GravityObject existingObject)
		{
			objects.Remove (existingObject);
		}

		void OnApplicationQuit()
		{
			objects = new List<GravityObject> ();
		}

		public void GravitationalPull(Rigidbody rb)
		{
				Vector3 normalForce = (rb.position - player.currentGround.transform.position).normalized;
				Vector3 localUp = rb.transform.up;

				rb.rotation = Quaternion.FromToRotation (localUp, normalForce) * rb.rotation;
		}

		public void FixedUpdate()
		{
			foreach (GravityObject target in objects)
			{
				target.GetComponent<Rigidbody>().AddExplosionForce (-gravity * target.gravityModifier, transform.position, radius, 0.0f, ForceMode.Acceleration);
			}
		}
	}
}